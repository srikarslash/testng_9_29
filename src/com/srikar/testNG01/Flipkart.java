package com.srikar.testNG01;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.collections.Lists;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

public class Flipkart {

	public String baseUrl1, baseUrl2, baseUrl3, baseUrl4, baseUrl5;

	// https://stackoverflow.com/questions/38624580/connect-selenium-test-to-saucelabs
	//https://examples.javacodegeeks.com/enterprise-java/testng/testng-dataprovider-example/
	//https://stackoverflow.com/questions/10832422/testng-more-than-one-dataprovider-for-one-test

	WebDriver driver;

	@DataProvider(name = "DriverInfoProvider", parallel = true) // data providers force single threaded by default
	public Object[][] driverInfoProvider() {
		return new Object[][] { 
				{ "firefox", "47.0", "Windows 10" }, 
				{ "chrome", "51.0", "Windows 10" },
				{ "Internet Explorer", "11", "Windows 10" }
				// etc, etc
		};
	}
	
	//Data provider uses an array without any external provider
	/*@DataProvider(name = "WebInfoProvider", parallel = true) // data providers force single threaded by default
	public Object[][] webInfoProvider() {
		return new Object[][] { 
				{ "https://www.google.com", "slash","Slash search"}, 
				{ "https://www.google.com", "sachin","Sachin search"},
				{ "https://www.google.com", "sukanth","Sukanth search"}
				// etc, etc
		};
	}*/
	
	
	//Data provider uses external json file.
	@DataProvider(name = "WebInfoProvider", parallel = true)
	public Object[][] webInfoProvider() throws FileNotFoundException{
		
		JsonElement jsonData = new JsonParser().parse(new FileReader("C:\\MyProjects\\TMobile_Gitlab1_0928\\TestNGProject02\\test.json"));
        JsonElement dataSet = jsonData.getAsJsonObject().get("dataSet");
        List<Data> testData = new Gson().fromJson(dataSet, new TypeToken<List<Data>>() {
        }.getType());
        Object[][] returnValue = new Object[testData.size()][1];
        int index = 0;
        for (Object[] each : returnValue) {
            each[0] = testData.get(index++);
        }
        return returnValue;
	}
	
	/*@DataProvider
	public Object[][] addTwoProviders(Object[] driverInfoProvider, Object[] webInfoProvider){
		List<Object[]> sum = Lists.newArrayList();
		
		int x = 0;
		int y = 10;
		
		
		
		for (int i=0;i<driverInfoProvider.length;i++) {
			x++;
			for(int j=0;j<webInfoProvider.length;j++) {
				y++;
				System.out.println(Arrays.toString(driverInfoProvider)+x+x+x);
				System.out.println(Arrays.toString(webInfoProvider)+y+y+y);
				
				
				Object[] both = (Object[])ArrayUtils.addAll(driverInfoProvider, webInfoProvider);
				sum.add(both);
				
			}
		}
		
		for(int k =0;k<sum.size();k++ ) {
			System.out.println(sum.get(k));
			
		}
		
		
		
		return sum.toArray(new Object[sum.size()][]) ;
		
		
	}*/
	
	
	/*@DataProvider
	public Object[][] dp() {
	  List<Object[]> result = Lists.newArrayList();
	  result.addAll(Arrays.asList(driverInfoProvider()));
	  result.addAll(Arrays.asList(webInfoProvider()));
	  return result.toArray(new Object[result.size()][]);
	}*/
	

	@Test(dataProvider = "WebInfoProvider")
	public void testFoo(Data data) {
		/*driver = Driver.createDriver(browser, version, os);
		searchInWeb(driver, webPage, search);*/
		
		driver = Driver.createDriver("firefox", "47.0", "Windows 10",data.getName());
		searchInWeb(driver, data.getWebPage() ,data.getSearch());

	}

	public void searchInWeb(WebDriver driver, String webPage, String search) {

		try {

			System.out.println("starting test in thread: " + Thread.currentThread().getName());
			driver.get(webPage);
			driver.findElement(By.name("q")).sendKeys(search);
			driver.findElement(By.name("q")).sendKeys(Keys.RETURN);
		}

		finally {
			driver.quit();
		}

	}

	

}
