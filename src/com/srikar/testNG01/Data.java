package com.srikar.testNG01;

public class Data {
	private String webPage;
	private String search;
	private String name;
	
	public String getWebPage() {
		return webPage;
	}
	public void setWebPage(String webPage) {
		this.webPage = webPage;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "Data [webPage=" + webPage + ", search=" + search + ", name=" + name + "]";
	}
	
	
}
